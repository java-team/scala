Description: specifying classpaths by hand instead of relying on Maven resolver
 as something got broken starting from maven-resolver/1.9.22
Author: Pierre Gruet <pgt@debian.org>
Bug-Debian: https://bugs.debian.org/1091067
Forwarded: not-needed
Last-Update: 2025-02-15

--- a/build.xml
+++ b/build.xml
@@ -257,204 +257,51 @@
     <path id="maven-ant-tasks.classpath" path="/usr/share/java/aether-ant-tasks.jar" />
     <typedef resource="org/eclipse/aether/ant/antlib.xml" uri="urn:maven-artifact-ant" classpathref="maven-ant-tasks.classpath" />
 
-    <!-- Resolve maven dependencies -->
+    <!-- Below, setting some paths and filesets by hand to avoid relying on Maven resolver, because of https://bugs.debian.org/1091067 -->
+    <path id="scala-java8-compat.libs">
+        <pathelement location="" />
+    </path>
+
+    <path id="external-modules-nocore">
+        <pathelement location="/usr/share/java/scala-xml_2.11.jar" />
+        <pathelement location="/usr/share/java/scala-parser-combinators_2.11.jar" />
+    </path>
+
+    <path id="starr.compiler.path">
+        <pathelement location="/usr/share/java/scala-compiler.jar" />
+        <pathelement location="/usr/share/java/scala-library.jar" />
+        <pathelement location="/usr/share/java/scala-parser-combinators_2.11.jar" />
+        <pathelement location="/usr/share/java/scala-reflect.jar" />
+        <pathelement location="/usr/share/java/scala-xml_2.11.jar" />
+    </path>
+
+    <path id="partest.classpath">
+        <pathelement location="" />
+    </path>
+
+    <path id="jarjar.classpath">
+        <pathelement location="/usr/share/java/jarjar.jar" />
+    </path>
+
+    <path id="asm.classpath">
+        <pathelement location="/usr/share/maven-repo/org/scala-lang/modules/scala-asm/debian/scala-asm-debian.jar" />
+    </path>
+
+    <path id="repl.deps.classpath">
+        <pathelement location="/usr/share/java/hawtjni-runtime.jar" />
+        <pathelement location="/usr/share/java/jansi-native.jar" />
+        <pathelement location="/usr/share/maven-repo/jline/jline/2.x/jline-2.x.jar" />
+    </path>
+
+    <path id="repl.fileset">
+        <pathelement location="/usr/share/java/hawtjni-runtime.jar" />
+        <pathelement location="/usr/share/java/jansi-native.jar" />
+        <pathelement location="/usr/share/maven-repo/jline/jline/2.x/jline-2.x.jar" />
+    </path>
 
-    <!-- work around http://jira.codehaus.org/browse/MANTTASKS-203:
-         java.lang.ClassCastException: org.codehaus.plexus.DefaultPlexusContainer cannot be cast to org.codehaus.plexus.PlexusContainer
-         on repeated use of artifact:dependencies
-    -->
-    <if><not><isset property="maven-deps-done"></isset></not><then>
-      <mkdir dir="${user.home}/.m2/repository"/>
-
-      <artifact:remoterepo id="sonatype-release" url="https://oss.sonatype.org/content/repositories/releases"/>
-      <artifact:remoterepo id="sonatype-snapshots" url="https://oss.sonatype.org/content/repositories/snapshots"/>
-      <artifact:remoterepo id="extra-repo" url="${extra.repo.url}"/>
-
-      <!-- This task has an issue where if the user directory does not exist, so we create it above. UGH. -->
-      <artifact:dependencies id="extra.tasks.deps">
-        <dependency coords="biz.aQute.bnd:bnd:debian"/>
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="extra.tasks.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <!--<localrepo path="${user.home}/.m2/repository"/>-->
-        <path refid="extra.tasks.classpath"/>
-        <files refid="extra.tasks.fileset"/>
-      </artifact:resolve>
-
-      <artifact:dependencies id="jarjar.deps">
-        <dependency coords="com.googlecode.jarjar:jarjar:debian"/>
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="jarjar.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="jarjar.classpath"/>
-      </artifact:resolve>
-
-      <artifact:dependencies id="jarlister.classpath">
-        <dependency coords="com.github.rjolly:jarlister_2.11:1.0"/>
-      </artifact:dependencies>
-
-      <!-- JUnit -->
-      <property name="junit.version" value="4.11"/>
-      <artifact:dependencies id="junit.deps">
-        <dependency coords="junit:junit:${junit.version}"/>
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="junit.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="junit.classpath"/>
-        <files refid="junit.fileset"/>
-      </artifact:resolve>
-      <copy-deps project="junit"/>
-
-      <!-- Pax runner -->
-      <property name="pax.exam.version" value="3.5.0"/><!-- Last version which supports Java 6 -->
-      <property name="osgi.felix.version" value="4.4.0"/>
-      <property name="osgi.equinox.version" value="3.7.1"/>
-      <artifact:dependencies id="pax.exam.deps">
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="pax.exam.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="pax.exam.classpath"/>
-        <files refid="pax.exam.fileset"/>
-      </artifact:resolve>
-      <!--<copy-deps project="pax.exam"/>-->
-
-      <artifact:dependencies id="osgi.framework.felix.deps">
-      </artifact:dependencies>
-
-      <artifact:dependencies id="osgi.framework.equinox">
-      </artifact:dependencies>
-
-      <artifact:remoterepo id="sonatype-release" url="https://oss.sonatype.org/content/repositories/releases"/>
-      <artifact:remoterepo id="extra-repo" url="${extra.repo.url}"/>
-
-      <!-- scala-java8-compat, used by the experimental -target jvm-1.8 support. -->
-      <if><isset property="scala-java8-compat.package"/><then>
-        <property name="scala-java8-compat.version" value="0.5.0"/>
-        <property name="scala-java8-compat.binary.version" value="2.11"/>
-        <artifact:dependencies pathid="scala-java8-compat.classpath" filesetId="scala-java8-compat.fileset">
-          <dependency coords="org.scala-lang.modules:scala-java8-compat_${scala-java8-compat.binary.version}:${scala-java8-compat.version}">
-            <exclusion groupId="org.scala-lang" artifactId="scala-library"/>
-          </dependency>
-        </artifact:dependencies>
-        <property name="scala-java8-compat-classes" value="${build-quick.dir}/scala-java8-compat"/>
-        <delete dir="${scala-java8-compat-classes}"/>
-        <unzip dest="${scala-java8-compat-classes}">
-          <fileset refid="scala-java8-compat.fileset"/>
-          <patternset>
-            <include name="**/*.class"/>
-          </patternset>
-        </unzip>
-        <path id="scala-java8-compat.libs">
-          <pathelement location="${scala-java8-compat-classes}"/>
-        </path>
-        <fileset id="scala-java8-compat.fileset" dir="${scala-java8-compat-classes}">
-          <include name="**/*"/>
-        </fileset>
-      </then>
-        <else>
-          <path id="scala-java8-compat.libs"/>
-          <fileset id="scala-java8-compat.fileset" dir="." excludes="**"/>
-        </else>
-      </if>
-
-      <!-- prepare, for each of the names below, the property "@{name}.cross", set to the
-           necessary cross suffix (usually something like "_2.11.0-M6". -->
-      <prepareCross name="scala-xml" />
-      <prepareCross name="scala-parser-combinators" />
-      <property name="scala-continuations-plugin.cross.suffix" value="_${scala.full.version}"/>
-      <prepareCross name="scala-continuations-plugin" />
-      <prepareCross name="scala-continuations-library"/>
-      <prepareCross name="scala-swing"/>
-      <prepareCross name="partest"/>
-      <prepareCross name="scalacheck"/>
-
-      <artifact:dependencies id="asm.deps">
-        <dependency coords="org.scala-lang.modules:scala-asm:${scala-asm.version}"/>
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="asm.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="asm.classpath"/>
-        <!--<files refid="asm.fileset.tmp"/>-->
-      </artifact:resolve>
-      <fileset id="asm.fileset" dir="debian/tmp/.m2/repository/org/scala-lang/modules/scala-asm" includes="**/*.jar"/>
-      <copy-deps project="asm"/>
-
-      <!-- TODO: delay until absolutely necessary to allow minimal build, also move out partest dependency from scaladoc -->
-      <artifact:dependencies id="partest.deps">
-      </artifact:dependencies>
-      <path id="partest.classpath"/>
-      <resource id="partest.fileset"/>
-      <copy-deps project="partest"/>
-
-      <artifact:dependencies id="scalacheck.deps">
-      </artifact:dependencies>
-
-      <artifact:dependencies id="repl.deps">
-        <dependency coords="jline:jline:${jline.version}"/>
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="repl.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="repl.deps.classpath"/>
-        <files refid="repl.fileset"/>
-      </artifact:resolve>
-      <copy-deps project="repl"/>
-
-      <!-- used by the test.osgi target to create osgi bundles for the xml, parser-combinator jars
-           must specify sourcesFilesetId, javadocFilesetId to download these types of artifacts -->
-      <artifact:dependencies id="external-modules.deps">
-        <dependency coords="org.scala-lang.modules:scala-xml${scala-xml.cross}:${scala-xml.version.number}"/>
-        <dependency coords="org.scala-lang.modules:scala-parser-combinators${scala-parser-combinators.cross}:${scala-parser-combinators.version.number}"/>
-<!--
-        <dependency groupId="org.scala-lang.plugins" artifactId="scala-continuations-plugin${scala-continuations-plugin.cross}"  version="${scala-continuations-plugin.version.number}"/>
-        <dependency groupId="org.scala-lang.plugins" artifactId="scala-continuations-library${scala-continuations-library.cross}" version="${scala-continuations-library.version.number}"/>
-        <dependency groupId="org.scala-lang.modules" artifactId="scala-swing${scala-swing.cross}" version="${scala-swing.version.number}"/>
--->
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="external-modules.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="external-modules.deps.classpath"/>
-        <files refid="external-modules.sources.fileset"/>
-      </artifact:resolve>
-
-      <!-- External modules, excluding the core -->
-      <path id="external-modules-nocore">
-        <restrict>
-          <path refid="external-modules.deps.classpath"/>
-          <rsel:not><rsel:or>
-            <rsel:name name="scala-library*.jar"/>
-            <rsel:name name="scala-reflect*.jar"/>
-            <rsel:name name="scala-compiler*.jar"/>
-          </rsel:or></rsel:not>
-        </restrict>
-      </path>
-      <copy-deps refid="external-modules-nocore" project="scaladoc"/>
-
-      <propertyForCrossedArtifact name="scala-parser-combinators" jar="org.scala-lang.modules:scala-parser-combinators"/>
-      <propertyForCrossedArtifact name="scala-xml"                jar="org.scala-lang.modules:scala-xml"/>
-      <propertyForCrossedArtifact name="scala-continuations-plugin"  jar="org.scala-lang.plugins:scala-continuations-plugin"/>
-      <propertyForCrossedArtifact name="scala-continuations-library" jar="org.scala-lang.plugins:scala-continuations-library"/>
-      <propertyForCrossedArtifact name="scala-swing"                 jar="org.scala-lang.modules:scala-swing"/>
-
-      <!-- BND support -->
-      <typedef resource="aQute/bnd/ant/taskdef.properties" classpathref="extra.tasks.classpath" />
-
-      <echo message="Using Scala ${starr.version} for STARR."/>
-      <artifact:dependencies id="starr.deps">
-        <!--<artifact:remoteRepository refid="extra-repo"/>-->
-        <dependency coords="org.scala-lang:scala-library:${starr.version}"/>
-        <dependency coords="org.scala-lang:scala-reflect:${starr.version}"/>
-        <dependency coords="org.scala-lang:scala-compiler:${starr.version}"/>
-      </artifact:dependencies>
-      <artifact:resolve dependenciesref="starr.deps">
-        <remoterepo id="central" url="file:///usr/share/maven-repo"/>
-        <path refid="starr.compiler.path"/>
-        <files refid="starr.fileset"/>
-      </artifact:resolve>
-      <copy-deps project="starr"/>
-
-      <property name="maven-deps-done"     value="yep!"/>
-    </then></if>
+    <fileset id="asm.fileset" dir="/usr/share/maven-repo/org/scala-lang/modules/scala-asm/debian/" includes="scala-asm-debian.jar" />
 
+    <fileset id="scala-java8-compat.fileset" dir="." excludes="**" />
 
     <!-- NOTE: ant properties are write-once: second writes are silently discarded; the logic below relies on this -->
 
